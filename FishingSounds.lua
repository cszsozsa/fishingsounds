
FishingSounds = {
    name = "FishingSounds"
}

local STATE_WAITING = 0
local STATE_FISHING =1
local STATE_GOT = 3
local STATE_CAUGHT = 4

local STATE_NAME = {
    "WAITING",
    "FISHING",
    "GOT",
    "CAUGHT"
}

local currentState

local function changeState(state, arg2)
    if currentState == state then return end

    if state == STATE_WAITING then
        if currentState == STATE_CAUGHT and not arg2 then return end

        EVENT_MANAGER:UnregisterForUpdate(FishingSounds.name .. "antiJobFictif")
		EVENT_MANAGER:UnregisterForEvent(FishingSounds.name .. "OnSlotUpdate", EVENT_INVENTORY_SINGLE_SLOT_UPDATE)
    elseif state == STATE_FISHING then
        EVENT_MANAGER:RegisterForEvent(FishingSounds.name .. "OnSlotUpdate", EVENT_INVENTORY_SINGLE_SLOT_UPDATE, FishingSounds.OnSlotUpdate)
    elseif state == STATE_GOT then
        -- https://wiki.esoui.com/Sounds For sounds
        PlaySound(SOUNDS.DUEL_START)

        EVENT_MANAGER:RegisterForUpdate(FishingSounds.name .. "antiJobFictif", 3000, function()
			if currentState == STATE_GOT then changeState(STATE_WAITING) end
		end)
    elseif state == STATE_CAUGHT then
        PlaySound(SOUNDS.DUEL_WON)

        EVENT_MANAGER:UnregisterForUpdate(FishingSounds.name .. "antiJobFictif")
        EVENT_MANAGER:UnregisterForEvent(FishingSounds.name .. "OnSlotUpdate", EVENT_INVENTORY_SINGLE_SLOT_UPDATE)
        
        EVENT_MANAGER:RegisterForUpdate(FishingSounds.name .. "champagne", 4000, function()
			if currentState == STATE_CAUGHT then changeState(STATE_WAITING, true) end
			EVENT_MANAGER:UnregisterForUpdate(FishingSounds.name .. "champagne")
		end)
    end

    currentState = state
end

function FishingSounds.OnSlotUpdate(event, bagId, slotIndex, isNew)
    if currentState == STATE_FISHING then
        changeState(STATE_GOT)
    elseif currentState == STATE_GOT then
        changeState(STATE_CAUGHT)
    end
end

local currentInteractableName

function FishingSounds.OnAction()
    local action, InteractableName, _, _, additionalInfo = GetGameCameraInteractableActionInfo()

    if action then
        local state = STATE_WAITING

        if additionalInfo == ADDITIONAL_INTERACT_INFO_FISHING_NODE then
            currentInteractableName = InteractableName

        elseif currentInteractableName == InteractableName then
            if currentState > STATE_FISHING then return end

            state = STATE_FISHING
        end

        changeState(state)
    elseif currentState ~= STATE_WAITING then
        changeState(STATE_WAITING)
    else

    end
    
end

function FishingSounds.OnAddOnLoad(eventCode, addonName)
    if addonName ~= FishingSounds.name then return end

    EVENT_MANAGER:UnregisterForEvent(FishingSounds.name, EVENT_ADD_ON_LOADED)

    ZO_PreHookHandler(RETICLE.interact, "OnEffectivelyShown", FishingSounds.OnAction)
    ZO_PreHookHandler(RETICLE.interact, "OnHide", FishingSounds.OnACtion)
end

EVENT_MANAGER:RegisterForEvent(FishingSounds.name, EVENT_ADD_ON_LOADED, function(...) FishingSounds.OnAddOnLoad(...) end)